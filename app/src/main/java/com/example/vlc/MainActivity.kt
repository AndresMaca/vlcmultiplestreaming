package com.example.vlc

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.streamingmodule.StreamingScreen
import com.example.streamingmodule.StreamingFactory
import com.example.vlc.databinding.ActivityMainBinding

class MainActivity :   AppCompatActivity() {

    private val binding by lazy(LazyThreadSafetyMode.NONE) {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private lateinit var screen1: StreamingScreen
    private lateinit var screen2: StreamingScreen
    private lateinit var screen3: StreamingScreen


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupPlayer()
        binding.releaseScreen.setOnClickListener { releaseScreensManually() }

        binding.resetScreen.setOnClickListener { initPlayerManually() }
    }


    private fun releaseScreensManually(){
        if(!screen1.isDetached() && !screen2.isDetached() && !screen3.isDetached()) {
            screen1.releaseAndDetachScreen()
            screen2.releaseAndDetachScreen()
            screen3.releaseAndDetachScreen()
        }
    }

    private fun initPlayerManually(){
        screen1.initPlayerAndPlay()
        screen2.initPlayerAndPlay()
        screen3.initPlayerAndPlay()
    }
    private fun setupPlayer() {
//"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4"
        val streamingURL = "rtsp://192.168.0.9:5540/ch0"

        screen1 = StreamingFactory.createLifecycleAwareScreen(this, binding.viewTopLeftVlcLayout,
            streamingURL)

        screen2 = StreamingFactory.createLifecycleAwareScreen(this, binding.viewLowerRigthVlcLayout,
            streamingURL, true)

        screen3 = StreamingFactory.createLifecycleAwareScreen(this, binding.viewTopRigthVlcLayout,
            streamingURL)
        lifecycle.addObserver(screen1)
        lifecycle.addObserver(screen2)
        lifecycle.addObserver(screen3)

    }
}