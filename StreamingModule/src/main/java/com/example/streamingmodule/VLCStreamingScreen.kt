package com.example.streamingmodule

import android.net.Uri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.example.streamingmodule.StreamingFactory.CLOCK_JITTER
import com.example.streamingmodule.StreamingFactory.CLOCK_SYNC
import com.example.streamingmodule.StreamingFactory.ENABLE_SUBTITLES
import com.example.streamingmodule.StreamingFactory.NETWORK_CACHING
import com.example.streamingmodule.StreamingFactory.USE_TEXTURE_VIEW
import org.videolan.libvlc.LibVLC
import org.videolan.libvlc.Media
import org.videolan.libvlc.MediaPlayer
import org.videolan.libvlc.util.VLCVideoLayout
import java.io.IOException

class VLCStreamingScreen (private var libVLC: LibVLC,
                          private val vlcVideoLayout: VLCVideoLayout,
                          private val streamingURL: String,
                          private val streamingOptions: MutableList<String>
                          ): StreamingScreen{

    private var mediaPlayer: MediaPlayer =
        MediaPlayer(libVLC)


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    override fun initPlayerAndPlay(){
        try{
            if (mediaPlayer.isReleased && libVLC.isReleased){
                libVLC = LibVLC(libVLC.appContext, streamingOptions)
                mediaPlayer = MediaPlayer(libVLC)
            }
            mediaPlayer.attachViews(
                vlcVideoLayout,
                null,
                ENABLE_SUBTITLES,
                USE_TEXTURE_VIEW
            )
            val uri = Uri.parse(streamingURL)
            Media(libVLC, uri).apply {
                setHWDecoderEnabled(false, false)
                addOption(NETWORK_CACHING)
                addOption(CLOCK_JITTER)
                addOption(CLOCK_SYNC)
                mediaPlayer.media = this
            }.release()
            mediaPlayer.play()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun detachMediaPlayer(){
        mediaPlayer.stop()
        mediaPlayer.detachViews()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun releaseVLCStreamingScreen(){
        mediaPlayer.release()
        libVLC.release()
    }

    override fun releaseAndDetachScreen() {
        if (isDetached()){
            detachMediaPlayer()
            releaseVLCStreamingScreen()
        }
    }

    override fun play() = mediaPlayer.play()

    override fun stop() = mediaPlayer.stop()


    override fun isDetached(): Boolean = mediaPlayer.isReleased && libVLC.isReleased
}
