package com.example.streamingmodule

import android.content.Context
import org.videolan.libvlc.LibVLC
import org.videolan.libvlc.util.VLCVideoLayout

object StreamingFactory {
     const val USE_TEXTURE_VIEW = false
     const val ENABLE_SUBTITLES = false

    const val CLOCK_JITTER = ":clock-jitter=0"
    const val CLOCK_SYNC = ":clock-synchro=0"
    const val NETWORK_CACHING = ":network-caching=500"

    fun createLifecycleAwareScreen(context: Context,
                                   vlcVideoLayout: VLCVideoLayout,
                                   streamingURL: String,
                                   mirrorMode: Boolean = false,
                                   vlcOptions: List<String> = listOf()): StreamingScreen
    {
        val streamingOptions = ArrayList<String>()
        if (mirrorMode)
            streamingOptions.add("--video-filter=transform{type=hflip}")
        if (vlcOptions.isNotEmpty())
            streamingOptions.addAll(vlcOptions)
        return VLCStreamingScreen(LibVLC(context, streamingOptions), vlcVideoLayout, streamingURL, streamingOptions)
    }
}