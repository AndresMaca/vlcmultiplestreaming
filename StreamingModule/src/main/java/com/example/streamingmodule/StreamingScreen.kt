package com.example.streamingmodule

import androidx.lifecycle.LifecycleObserver

interface StreamingScreen: LifecycleObserver {
    fun play()
    fun stop()
    fun isDetached(): Boolean
    fun initPlayerAndPlay()
    fun releaseAndDetachScreen()
}